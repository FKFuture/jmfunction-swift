//
//  JMDecimalNumberTool.swift
//  REC
//
//  Created by JM on 2019/12/5.
//  Copyright © 2019 xinlianyun. All rights reserved.
//

import UIKit

//数据类型转换处理
class JMDecimalNumberTool: NSObject {

    /*
     //    value 1.2  1.21  1.25  1.35  1.27
     // Plain    1.2  1.2   1.3   1.4   1.3
     // Down     1.2  1.2   1.2   1.3   1.2
     // Up       1.2  1.3   1.3   1.4   1.3
     // Bankers  1.2  1.2   1.2   1.4   1.3
     */


    //使用double返回float类型数据
    class  func floatWithdecimalNumber(num:Double) -> Float {

        let doubleString:String = "\(num)"

        let decimal:NSDecimalNumber = NSDecimalNumber.init(string: doubleString)

        return decimal.floatValue
    }

    //使用double返回string类型数据(使用double类型数据返回的字符无截取)
    class  func stringWithdecimalNumber(num:Double) -> String {

        let doubleString:String = "\(num)"

        let decimal:NSDecimalNumber = NSDecimalNumber.init(string: doubleString)

        return decimal.stringValue
    }


    //使用string返回double类型数据
    class  func doubleWithdecimalNumber(data:String) -> Double {

        let decimal:NSDecimalNumber = NSDecimalNumber.init(string: data)

        return decimal.doubleValue

    }

    //版本号比较
    class  func versionCompare(currentVersion:String,lastestVersion:String) -> Bool {

        let currentString = self.getDoubleString(content: currentVersion)
        let lastestString = self.getDoubleString(content: lastestVersion)

        print(self.doubleWithdecimalNumber(data: currentString))
        print(self.doubleWithdecimalNumber(data: lastestString))


        if  self.doubleWithdecimalNumber(data: currentString) >= self.doubleWithdecimalNumber(data: lastestString) {

            return false
        }

        return true

    }

    //字符处理
    class func getDoubleString(content:String) -> String{

        let firstPointIndex = self.positionOf(superString: content, sub: ".")
        var currentString = content.replacingOccurrences(of: ".", with: "")
        let start = currentString.index(currentString.startIndex, offsetBy: firstPointIndex)

        //插入一段内容，两个参数：插入的起点和用来插入的内容
        currentString.insert(contentsOf: ".", at: start)

        return currentString

    }


 //获取字符在某个字符串中的第一个位置
    class   func positionOf(superString:String,sub:String, backwards:Bool = false)->Int {
        var pos = -1
        if let range = superString.range(of:sub, options: backwards ? .backwards : .literal ) {
            if !range.isEmpty {
                pos = superString.distance(from:superString.startIndex, to:range.lowerBound)
            }
        }
        return pos
    }



    /*
    //金钱计算api
    - (void)moenyCount{

    NSString *moneyString = @" 1";
    CGFloat convert = 2.31765;

    //字符类型转NSDecimalNumber
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:moneyString];

    //基础类型转NSDecimalNumber
    NSDecimalNumber *countNumber = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:convert] decimalValue]];


    //设置
    NSDecimalNumberHandler *handel = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];

    //    NSRoundBankers 保留小数是采取五入
    //    NSRoundPlain 保留小数是采取五入
    //    NSRoundDown 是舍去保留位数后面的数
    //    NSRoundUp   保留小数是采取对最后一位加1



    //+运算
    NSDecimalNumber *addNumber = [number decimalNumberByAdding:countNumber];
    NSDecimalNumber *addNumber2 = [number decimalNumberByAdding:countNumber withBehavior:handel];


    //-运算
    NSDecimalNumber *reduceNumber = [number decimalNumberBySubtracting:countNumber];



    //乘法
    NSDecimalNumber *multiByNumber = [number decimalNumberByMultiplyingBy:countNumber];



    //除法
    NSDecimalNumber *diviNumber = [number decimalNumberByDividingBy:countNumber];



    NSLog(@"黄鹤楼讲话稿");


    }
    */





}
