//
//  JMClassChangeTool.swift
//  REC
//
//  Created by JM on 2019/12/14.
//  Copyright © 2019 xinlianyun. All rights reserved.
//

import UIKit

//类型转换处理
class JMClassChangeTool: NSObject {

    //字符串转字典
  class  func stringValueDic(_ str: String) -> [String : Any]?{

    let data = str.data(using: String.Encoding.utf8)
    if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
        return dict
    }
    return (NSDictionary() as! [String : Any])
    
    }


    //字典转字符串
class    func dicValueString(dict:NSDictionary?)->String{

        if dict != nil {

            let data = try? JSONSerialization.data(withJSONObject: dict!, options: JSONSerialization.WritingOptions.prettyPrinted)

            let strJson = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

            return strJson! as String

        }

        return ""

    }

    //数组转字符串
    class    func arrayValueString(array:NSArray?)->String{

        if array != nil {

            let data = try? JSONSerialization.data(withJSONObject: array!, options: JSONSerialization.WritingOptions.prettyPrinted)

            let strJson = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

            return strJson! as String

        }

        return ""

    }

    //json字符串转数组
    class    func stringValueArray(_ str: String)->Array<Any>{

        let data = str.data(using: String.Encoding.utf8)
        if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Any> {
            return dict
        }
        return NSArray() as! Array<Any>

    }







}
