//
//  JMPhotoManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/16.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMPhotoManager: NSObject {
    
    func showPhotoAction(title:String,photoTitle:String,cameraTitle:String,currentVC:UIViewController) {
        
        let alertVC = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        let cancleAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        let photoAction = UIAlertAction(title: photoTitle, style: .default) { action in
            
            self.openPhoto(currentVC: currentVC)
        }
        
        let cameraAction = UIAlertAction(title: cameraTitle, style: .default) { action in
            
            self.openCamera(currentVC: currentVC)
        }
        
        alertVC.addAction(cancleAction)
        alertVC.addAction(photoAction)
        alertVC.addAction(cameraAction)
        
        currentVC.present(alertVC, animated: true, completion: nil)

        
    }
    
    func openCamera(currentVC:UIViewController) {
        
        let picker = UIImagePickerController()
        picker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        picker.allowsEditing = true
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            picker.sourceType = .camera
            currentVC.present(picker, animated: true, completion: nil)
        }else{
            
            NSLog("相机不可用")
        }
        
    }
    
    func openPhoto(currentVC:UIViewController)  {
        
        let picker = UIImagePickerController()
        picker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        picker.allowsEditing = true
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            picker.sourceType = .photoLibrary
            currentVC.present(picker, animated: true, completion: nil)
        }else{
            
            NSLog("手机相册不可用")
        }

        
    }
    
    

}
