//
//  JMStringManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/9/13.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMStringManager: NSObject {
    
    func getArrayfromString(originString:String) -> Array<String> {
        
        let resultArr = Array(originString)
        let returnArr = NSMutableArray()
        
        for i in 0..<resultArr.count {
            
            let content:String = String(resultArr[i])
            returnArr.add(content)

        }
        
        return returnArr.copy() as! Array<String>


    }
    

}
