//
//  JMRegularManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/16.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMRegularManager: NSObject {
    
    //密码由6-8位组成，包含数字和字母。
    class   func passwordFormatIsSure(text: String) -> Bool{

           let allRegex:NSPredicate = NSPredicate(format: "SELF MATCHES %@", "^[\\x21-\\x7E]{6,8}$")
           let numberRegex:NSPredicate = NSPredicate(format: "SELF MATCHES %@", "^.*[0-9]+.*$")
           let letterRegex:NSPredicate = NSPredicate(format: "SELF MATCHES %@", "^.*[A-Za-z]+.*$")

           if numberRegex.evaluate(with: text) && letterRegex.evaluate(with: text) && allRegex.evaluate(with: text){

               return true
           }

           return false

       }


}
