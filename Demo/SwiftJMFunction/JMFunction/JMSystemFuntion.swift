//
//  JMSystemFuntion.swift
//  REC
//
//  Created by JM on 2019/12/7.
//  Copyright © 2019 xinlianyun. All rights reserved.
//

import UIKit

//系统函数
class JMSystemFuntion: NSObject {


    //弧度转角度
 class   func getDegreesFromValue(value:CGFloat) -> CGFloat {


       return value * (CGFloat.pi/180.0)


    }

    //角度转弧度

 class   func getValueFromDegree(degree:CGFloat) -> CGFloat {

      print(CGFloat.pi)


      return degree * (CGFloat.pi/180.0)

    }

   //获取相应角度对应的正弦值
    class   func getSinValue(degree:CGFloat) -> CGFloat {

        //先把度数转为弧度
        let value = JMSystemFuntion.getValueFromDegree(degree: degree)

        return sin(value)

    }

    //获取对应角度的余弦值
    class   func getCosValue(degree:CGFloat) -> CGFloat {

        //先把度数转为弧度
        let value = JMSystemFuntion.getValueFromDegree(degree: degree)

        return cos(value)

    }

    //获取对应弧度的正弦值
    class   func getSinValue(value:CGFloat) -> CGFloat {


        return sin(value)

    }

    //获取对应弧度的余弦值
    class   func getCosValue(value:CGFloat) -> CGFloat {


        return cos(value)

    }

    //获取最大值
    class   func getMaxValue(valueArray:Array <CGFloat>) -> CGFloat {

        let maxNum = valueArray.max()

        return maxNum!

    }

    //拉伸图片(以中心点拉伸)
    class   func stretchableImage(image:UIImage) -> UIImage {

        var getImage = image

        let leftCap = image.size.width/2.0
        let topCap = image.size.height/2.0

        getImage = getImage.stretchableImage(withLeftCapWidth: Int(leftCap), topCapHeight: Int(topCap))

        return getImage

    }

    //设置 只拉伸内容，保持边距UIEdgeInsetsMake
    class   func resizableImageWithCapInsets(image:UIImage,inset:UIEdgeInsets) -> UIImage {

       let getImage = image.resizableImage(withCapInsets: inset, resizingMode: UIImage.ResizingMode.stretch)

        return getImage

    }

    //在文本已经赋值的情况下这样计算更精确
    class   func countLabelFitSize(font:UIFont,text:String,maxSize:CGSize) -> CGSize {

        let label = UILabel.init()
        label.font = font
        label.text = text

        let size = label.sizeThatFits(maxSize)

        return size

    }





}
