//
//  JMLabelManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/19.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMLabelManager: NSObject {
    
    class func setLabelfirstLineHeadIndent(label:UILabel,characterCount:Int) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.firstLineHeadIndent = (label.font.pointSize)*CGFloat(characterCount)
        
        let rang = NSRange(location: 0, length: label.text?.count ?? 0)

        let attributed:NSMutableAttributedString = label.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.paragraphStyle:paragraphStyle], range: rang)

        
        label.attributedText = attributed

    }
    
    class func changeLabelLineMargin(label:UILabel,paragraphStyle:NSMutableParagraphStyle) {
                
        let rang = NSRange(location: 0, length: label.text?.count ?? 0)

        let attributed:NSMutableAttributedString = label.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.paragraphStyle:paragraphStyle], range: rang)

        
        label.attributedText = attributed

    }


}
