//
//  JMLocalManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/16.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMLocalManager: NSObject {
    
    func saveDefault(key:String,value:AnyObject?)  {

        if value != nil{

            UserDefaults.standard.set(value, forKey: key)
            UserDefaults.standard.synchronize()
        }

    }

    func removeUserDefault(key:String?)  {

        if key != nil{

            UserDefaults.standard.removeObject(forKey: key!)
            UserDefaults.standard.synchronize()
        }

    }

    func getUserDefault(key:String) ->AnyObject {


        return UserDefaults.standard.value(forKey: key) as AnyObject

    }


}
