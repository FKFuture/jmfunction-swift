//
//  JMCodeManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/16.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMCodeManager: NSObject {
    
    class   func setupQRCodeImage(_ text: String,width:CGFloat) -> UIImage {
            //创建滤镜
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setDefaults()
            //将url加入二维码
            filter?.setValue(text.data(using: String.Encoding.utf8), forKey: "inputMessage")
            //取出生成的二维码（不清晰）
            if let outputImage = filter?.outputImage {
                //生成清晰度更好的二维码
                let qrCodeImage = setupHighDefinitionUIImage(outputImage, size: width)

                return qrCodeImage
            }

            return UIImage()
    }
    
    class    func setupHighDefinitionUIImage(_ image: CIImage, size: CGFloat) -> UIImage {
        
        
        let integral: CGRect = image.extent.integral
        let proportion: CGFloat = min(size/integral.width, size/integral.height)

        let width = integral.width * proportion
        let height = integral.height * proportion
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceGray()
        let bitmapRef = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: 0)!

        let context = CIContext(options: nil)
        let bitmapImage: CGImage = context.createCGImage(image, from: integral)!

        bitmapRef.interpolationQuality = CGInterpolationQuality.none
        bitmapRef.scaleBy(x: proportion, y: proportion);
        bitmapRef.draw(bitmapImage, in: integral);
        let image: CGImage = bitmapRef.makeImage()!
        return UIImage(cgImage: image)
        
    }



}
