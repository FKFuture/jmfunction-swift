//
//  JMArrayManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/16.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit


extension Array{
    
    //数组乱序
    mutating func randamArray() -> Array {
        
        var list = self
        for index in 0..<list.count {
            let newIndex = Int(arc4random_uniform(UInt32(list.count-index))) + index
            if index != newIndex {
                
                list.swapAt(index, newIndex)
            }
        }
        
        self = list
        return self
    }

}


