//
//  JMFileManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMFileManager: NSObject {
    
    
    /*获取沙盒主目录路径*/
    public func homeDir() -> String{
        return NSHomeDirectory()
    }
    
    /*app路径*/
    public func  appDir() -> String{
        let arrays = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.applicationDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return arrays[0]
    }
    
    /*获取Documents目录路径*/
    public func  docDir() -> String{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
    }
    
    /*Library*/
    public func  libraryDir() -> String{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
    }
    
    /*获取Caches目录路径*/
    public func  cachesDir() -> String{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
    }
    
    /*获取tmp目录路径*/
    public func  tmpDir() -> String{
        return NSTemporaryDirectory()
    }
    
    /// 根据父文件夹路径和文件名得到文件路径
    /// - Parameters:
    ///   - fileName: 文件名
    ///   - folderPath:父文件夹路径
    /// - Returns: 返回当前文件路径
    private func getFilePath(fileName : String , folderPath : String) -> String{
        
        if fileName == ""{
            return folderPath
        }
        return folderPath + "/" + fileName
    }

    

    
    /*文件是否存在*/
    public func  isFileExisted(filepath : String) -> Bool{
        
        return FileManager.default.fileExists(atPath: filepath)
    }
    
    
    /*创建指定名字的文件夹*/
    public func  createFolder(folderName : String = "", path : String ,withIntermediateDirectories : Bool = true, attributes : [FileAttributeKey : Any]? = nil) -> Bool{
        
        if FileManager.default.fileExists(atPath: path) == false{
            do {
                try FileManager.default.createDirectory(atPath: getFilePath(fileName: folderName, folderPath: path), withIntermediateDirectories: withIntermediateDirectories, attributes: attributes)
            }catch{
                return false
            }
            return true
        }
        return false
    }


    
    /*创建指定名字的文件*/
    public func  createFile(fileName : String = "", path : String , contents : Data? = nil , attributes : [FileAttributeKey : Any]? = nil) -> Bool{
        if FileManager.default.fileExists(atPath: path) == false{
            return FileManager.default.createFile(atPath: getFilePath(fileName: fileName, folderPath: path), contents: contents, attributes: attributes)
        }
        return false
    }
    
    /*获取某个目录下所有的文件路径*/
    public func  getAllfilePathsInFolder(folderPath : String) -> [String]{
        do{
            let tmpFiles = try FileManager.default.contentsOfDirectory(atPath: folderPath)
            var files = [String]()
            for fileName in tmpFiles{
                files.append(folderPath + "/" + fileName)
            }
            return files
        }catch{
            return []
        }
    }
    
    /*获取某个目录下所有的文件名字*/
    public func  getAllfileNameInFolder(folderPath : String) -> [String]{
        do{
            let tmpFiles = try FileManager.default.contentsOfDirectory(atPath: folderPath)
            return tmpFiles
        }catch{
            return []
        }
    }
    
    /*删除目录下的所有文件*/
    public func  deleteFilesInFolder(folderPath : String) -> Bool{
        
        let fileList = getAllfilePathsInFolder(folderPath: folderPath)
        
        for file in fileList{
            
            if deleteFile(folderPath: file) == false{
                return false
            }
        }
        return true
    }
    
    /*删除文件*/
    public func  deleteFile(fileName : String = "", folderPath : String) -> Bool{
        if FileManager.default.fileExists(atPath: getFilePath(fileName: fileName, folderPath: folderPath)){
            do{
                try FileManager.default.removeItem(atPath: getFilePath(fileName: fileName, folderPath: folderPath))
            }catch{
                return false
            }
            return true
        }
        return false
    }
    
    /*根据url删除文件*/
    public func  deleteFileWithUrl(url : URL) -> Bool{
        do{
            try FileManager.default.removeItem(at: url)
        }catch{
            return false
        }
        return true
    }
    
    /*移动文件*/
    public func  moveFile(fromFilePath : String , toFilePath:String) -> Bool{
        do{
            try FileManager.default.moveItem(atPath: fromFilePath, toPath: toFilePath)
        }catch{
            return false
        }
        return true
    }
    
    /*根据url移动文件*/
    public func  moveFile(atUrl : URL , toUrl : URL) -> Bool{
        do{
            try FileManager.default.moveItem(at: atUrl, to: toUrl)
        }catch{
            return false
        }
        return true
    }
    
    /*复制文件*/
    public func  copyFile(fromFilePath : String , toFilePath:String) -> Bool{
        do{
            try FileManager.default.copyItem(atPath: fromFilePath, toPath: toFilePath)
        }catch{
            return false
        }
        return true
    }
    
    /*根据url复制文件*/
    public func  copyFile(atUrl : URL , toUrl : URL) -> Bool{
        do{
            try FileManager.default.copyItem(at: atUrl, to: toUrl)
        }catch{
            return false
        }
        return true
    }



    func writeDicToFile(dataDic:Dictionary<String, Any>,filePath:String) -> Bool {
        
        let writeDic:NSMutableDictionary = dataDic as! NSMutableDictionary
        
        let write :Bool =  writeDic.write(toFile: filePath, atomically: true)
        
        if write {
            
            print("写入成功")
            
        }else{

            print("写入失败")
        }

        
        return write

    }
    
    /// 返回可以使用的文件路径
    /// - Parameter fileName: 文件名，需要后缀，如Config.plist
    /// - Returns:
    func getDefalutFilePathWithName(fileName:String) -> String {
        
        let basePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        let filePath = basePath + "/" + fileName
        
        return filePath
        
    }

    




}
