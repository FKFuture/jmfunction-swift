//
//  JMImageManager.swift
//  SwiftJMFunction
//
//  Created by JM on 2022/8/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMImageManager: NSObject {
    
    //以中心点拉伸图片
    func stretchableImageByCenter(image:UIImage) -> UIImage {
        
        var getImage = image

        let leftCap = image.size.width/2.0
        let topCap = image.size.height/2.0

        getImage = getImage.stretchableImage(withLeftCapWidth: Int(leftCap), topCapHeight: Int(topCap))

        return getImage


    }
    
    //拉伸图片
    func stretchableImageByInset(image:UIImage,inset:UIEdgeInsets) -> UIImage {
        
        let getImage = image.resizableImage(withCapInsets: inset, resizingMode: UIImage.ResizingMode.stretch)

         return getImage


    }
    
    //保存图片
    func saveImage(image:UIImage) {
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject)  {

        if error != nil {

            NSLog("保存失败");
            return
        }
        
        NSLog("已保存至相册");


    }

    


}
